import networkx as nx 
from lxml import etree as ET
import numpy as np
import math 
from scipy.spatial import distance
from os import walk
import operator
import sys
import time
from scipy.optimize import linear_sum_assignment


def read_graph_from_xml(file_path):
    G = nx.Graph()
    nodes={}
    tree = ET.parse(file_path)
    root = tree.getroot()
    id = 0
    for graph in root:
        for element in graph:
            if element.tag == "node":
                nodes[str(element.attrib['id'])]=id
                i = 0
                x_v=y_v=0
                for attrib in element:
                    for value in attrib:
                        if i==0:
                            x_v=float(value.text)
                        else:
                            y_v=float(value.text)
                        i+=1
                G.add_node(id,x=x_v,y=y_v)
                id+=1
            elif element.tag=="edge" :# it's an edge    
                source = nodes[str(element.attrib['from'])]
                target = nodes[str(element.attrib["to"])]
                #distance = float(element.attrib["weight"])
                distance = math.sqrt(math.pow(G.nodes[source]['x']-G.nodes[target]['x'],2) + math.pow(G.nodes[source]['y']-G.nodes[target]['y'],2))
                G.add_edge(source,target,weight=distance)
    #distance = math.sqrt(math.pow(G.nodes[0]['x']-G.nodes[len(G.nodes())-1]['x'],2) + math.pow(G.nodes[0]['y']-G.nodes[len(G.nodes())-1]['y'],2))           
    #G.add_edge(0,len(G.nodes())-1,weight=distance)
    return G


def graph_to_xml(G,file_path):
    gxl = ET.Element("gxl")
    graph_xml = ET.SubElement(gxl,"graph")
    for n in G.nodes():
        node_xml = ET.SubElement(graph_xml,"node", id =str(n))
        attrib =   ET.SubElement(node_xml,"attr", name =str("x"))
        x_cord =   ET.SubElement(attrib,"float").text = str(G.nodes[n]['x'])
        attrib =   ET.SubElement(node_xml,"attr", name =str("y"))
        y_cord =   ET.SubElement(attrib,"float").text = str(G.nodes[n]['y'])
    i = 0
    for e in G.edges():
        edge_xml = ET.SubElement(graph_xml,"edge", id =str(i),source=str(e[0]),to = str(e[1]))#,weight = str(G[e[0]][e[1]]["weight"]))
        i+=1
    tree = ET.ElementTree(gxl)
    tree.write(file_path,pretty_print=False)

def euclidean_distance(a,b):
    return math.sqrt(math.pow(a[0]-b[0],2) + math.pow(a[1]-b[1],2))


def compute_distance_to_set(point,X):
    d = float('inf')
    for id in X:
        if (d>euclidean_distance(point,X[id])):
            d=euclidean_distance(point,X[id])
    return d


def greedy_diversity_selection(S,m):
    Sel = {}
    number_of_nodes = len(list(S))
    while len(Sel) < m and len(Sel) < number_of_nodes :
        d = {}
        for s in S:
            if s not in Sel:
                d[s] = compute_distance_to_set(S[s],Sel) 
        node_to_add = max(d.items(), key=operator.itemgetter(1))[0]
        Sel[node_to_add] = S[node_to_add]
        del S[node_to_add]
    return Sel



def graph_longest_path(G):
    path=[]
    for n in G.nodes():
        if len(list(G.neighbors(n)))>1:
            continue
        marked = []
        new_path = longest_path(G,n,marked)
        if  len(path) < len(new_path):
            path = new_path		
    return path
	
def longest_path(G, n, marked):
	marked_copy = marked 
	marked_copy.append(n)
	neighors = G.neighbors(n)
	path  = []
	for n2 in neighors:
		if ( n2 in marked_copy):
			continue
		new_path = longest_path(G,n2,marked_copy)
		if (len(new_path)>len(path)):
			path = new_path
	path.append(n)
	return path

def construct_graph_from_path(G,path):
    path_graph = nx.Graph()
    for n in path:
        path_graph.add_node(n,x=G.nodes[n]['x'],y=G.nodes[n]['y'])
    for i in range(len(path)-1):
        distance =  math.sqrt(math.pow(G.nodes[path[i]]['x']-G.nodes[path[i+1]]['x'],2) + math.pow(G.nodes[path[i]]['y']-G.nodes[path[i+1]]['y'],2)) 
        path_graph.add_edge(path[i],path[i+1],weight=distance)
    return path_graph




def angle_between(G,p1,p2,p3):
    a = math.sqrt(math.pow(G.nodes[p1]['x']-G.nodes[p3]['x'],2) + math.pow(G.nodes[p1]['y']-G.nodes[p3]['y'],2)) 
    b = math.sqrt(math.pow(G.nodes[p2]['x']-G.nodes[p3]['x'],2) + math.pow(G.nodes[p2]['y']-G.nodes[p3]['y'],2)) 
    c = math.sqrt(math.pow(G.nodes[p1]['x']-G.nodes[p2]['x'],2) + math.pow(G.nodes[p1]['y']-G.nodes[p2]['y'],2))
    d = round((b*b+c*c-a*a)/(2*b*c),4)
    ang_rd = math.acos(d)
    angle = math.degrees(ang_rd)
    return angle


        

def Sparsification(G,longest_p,nb_points):
    normalized_path=[]
    S={}
    Sel={}
    for n in longest_p:
        S[n] = np.array([G.nodes[n]['x'],G.nodes[n]['y']])
    Sel = greedy_diversity_selection(S,nb_points)
    for n in longest_p:
        if (n in Sel):
            normalized_path.append(n)
    new_graph = construct_graph_from_path(G,normalized_path)
    return new_graph


def paths_to_vec(G,paths):
    mat={}
    for i in paths:
        vec = []
        path = paths[i]
        pred = -1
        pred2 = -1
        vec.append(1)
        for p in path: 
            if (pred!=-1 and pred2!=-1):
                vec.append(angle_between(G,pred2,pred,p)) 
            pred2=pred
            pred = p
        mat[i]=vec
        print(vec)
    return mat




if __name__ == "__main__":
    
    sys.setrecursionlimit(10000)
    input_graphs_directory = sys.argv[1]  
    output_longest_paths_directory = sys.argv[2]  
    output_sparsified_paths_directory = sys.argv[3]  
    nb_points = int(sys.argv[4])  
    print("reading graphs and computing spasified graphs")
    for (dirpath, dirname, filenames) in walk(input_graphs_directory):
        for filename in filenames:
            print(filename)
            G = read_graph_from_xml(input_graphs_directory+"\\"+filename) 
            longest_p = graph_longest_path(G)                   
            longest_path_graph = construct_graph_from_path(G,longest_p)
            sparsified_path = Sparsification(G,longest_p,nb_points)
            graph_to_xml(longest_path_graph,output_longest_paths_directory+"\\"+filename)
            graph_to_xml(sparsified_path,output_sparsified_paths_directory+"\\"+filename)