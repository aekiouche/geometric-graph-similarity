import networkx as nx 
from lxml import etree as ET
import numpy as np
import math 
from scipy.spatial import distance
from os import walk
import operator
import sys
import time
from scipy.optimize import linear_sum_assignment


def read_graph_from_xml(file_path):
    G = nx.Graph()
    nodes={}
    tree = ET.parse(file_path)
    root = tree.getroot()
    id = 0
    for graph in root:
        for element in graph:
            if element.tag == "node":
                nodes[str(element.attrib['id'])]=id
                i = 0
                x_v=y_v=0
                for attrib in element:
                    for value in attrib:
                        if i==0:
                            x_v=float(value.text)
                        else:
                            y_v=float(value.text)
                        i+=1
                G.add_node(id,x=x_v,y=y_v)
                id+=1
            elif element.tag=="edge" :# it's an edge    
                source = nodes[str(element.attrib["from"])]
                target = nodes[str(element.attrib["to"])]
                #distance = float(element.attrib["weight"])
                distance = math.sqrt(math.pow(G.nodes[source]['x']-G.nodes[target]['x'],2) + math.pow(G.nodes[source]['y']-G.nodes[target]['y'],2))
                G.add_edge(source,target,weight=distance)
    return G

def Dwivedi_GDM(G1,G2,w1,w2,w3,w4):
   return VD(G1,G2,w1)+EDM(G1,G2,w2,w3,w4)

def E_A(ai,bi,ap,bp,xj,yj,xq,yq):
    if ( math.sqrt(pow(ap-ai,2)+pow(bp-bi,2))==0) :
        Oi =0
    else:
        Oi = np.arcsin(math.sqrt(pow(ap-ap,2)+pow(bp-bi,2))/math.sqrt(pow(ap-ai,2)+pow(bp-bi,2)))
    if (math.sqrt(pow(xq-xj,2)+pow(yq-yj,2))==0):
        Oj=0
    else:
        Oj = np.arcsin(math.sqrt(pow(xq-xq,2)+pow(yq-yj,2))/math.sqrt(pow(xq-xj,2)+pow(yq-yj,2)))
    return abs(Oi-Oj)

def E_L(ai,bi,ap,bp,xj,yj,xq,yq):
    li = math.sqrt(pow(ap-ai,2)+pow(bp-bi,2))
    lj = math.sqrt(pow(xq-xj,2)+pow(yq-yj,2))
    return abs(li-lj)

def E_P(ai,bi,ap,bp,xj,yj,xq,yq):
    ep = ( math.sqrt(pow(ai-xj,2)+pow(bi-yj,2)) + math.sqrt(pow(ap-xq,2)+pow(bp-yq,2)))/2
    return ep

def EDM(G1,G2,w2,w3,w4):
    cost_matrix = []
    for e1 in G1.edges():
        ai = G1.nodes[e1[0]]['x']
        bi = G1.nodes[e1[0]]['y']
        ap = G1.nodes[e1[1]]['x']
        bp = G1.nodes[e1[1]]['y']
        row=[]
        for e2 in G2.edges():
            xj = G2.nodes[e2[0]]['x']
            yj = G2.nodes[e2[0]]['y']
            xq = G2.nodes[e2[1]]['x']
            yq = G2.nodes[e2[1]]['y']
            edm =   E_A(ai,bi,ap,bp,xj,yj,xq,yq) \
                  + E_L(ai,bi,ap,bp,xj,yj,xq,yq) \
                  + E_P(ai,bi,ap,bp,xj,yj,xq,yq)
            row.append(edm)
        cost_matrix.append(row)
    cost_matrix = np.array(cost_matrix)
    row_ind, col_ind = linear_sum_assignment(cost_matrix)
    EDM_distance = 0
    le1 = list(G1.edges())
    le2 = list (G2.edges())
    for i in range(0,row_ind.shape[0]):
        e1 = le1[row_ind[i]]
        e2 = le2[col_ind[i]]
        ai = G1.nodes[e1[0]]['x']
        bi = G1.nodes[e1[0]]['y']
        ap = G1.nodes[e1[1]]['x']
        bp = G1.nodes[e1[1]]['y']
        xj = G2.nodes[e2[0]]['x']
        yj = G2.nodes[e2[0]]['y']
        xq = G2.nodes[e2[1]]['x']
        yp = G2.nodes[e2[1]]['y']
        EDM_distance +=   w2*E_A(ai,bi,ap,bp,xj,yj,xq,yq) \
                  + w3*E_L(ai,bi,ap,bp,xj,yj,xq,yq) \
                  + w4*E_P(ai,bi,ap,bp,xj,yj,xq,yq)

    return EDM_distance

def VD(G1,G2,w1):
    cost_matrix = []
    for v1 in G1.nodes():
        ai = G1.nodes[v1]['x']
        bi = G1.nodes[v1]['y']
        row=[]
        for v2 in G2.nodes():
            xj = G2.nodes[v2]['x']
            yj = G2.nodes[v2]['y']
            vd = math.sqrt(pow(ai-xj,2)+pow(bi-yj,2))
            row.append(vd)
        cost_matrix.append(row)
    cost_matrix = np.array(cost_matrix)
    row_ind, col_ind = linear_sum_assignment(cost_matrix)
    return w1*cost_matrix[row_ind, col_ind].sum()



if __name__ == "__main__":
    sys.setrecursionlimit(10000)
    original_graph_path = sys.argv[1]  
    output_file_result = sys.argv[2]
    nb_items_per_class =  int(sys.argv[3]) 
    
    class_labels={0:"C1",1:"C2",2:"C3",3:"C4",4:"C5",5:"C6",6:"C7",7:"C8",8:"C9",9:"C10"}

    dict_graphs = {}



    
    dict_graphs = {}
    print("reading graphs")
    start = time.time()
    for (dirpath, dirname, filenames) in walk(original_graph_path):
        for filename in filenames:
            GG = read_graph_from_xml(original_graph_path+"\\"+filename)
            dict_graphs[filename]= GG
    print("computing distance matrix")
    mat_distance = {}
    start = time.time()
    dist_map={}
    list_graphs_names = list(dict_graphs.keys())
    for i in range(len(list_graphs_names)):
        dist_map[i]={}
    for i in range(len(list_graphs_names)):
        print(i)
        dist_map[i][i]=0
        for j in range(i+1,len(list_graphs_names)):
            dij = Dwivedi_GDM(dict_graphs[list_graphs_names[i]],dict_graphs[list_graphs_names[j]],0.35,0.23,0.11,0.31)
            dist_map[i][j]=dij
            dist_map[j][i]=dij

    for i in range(len(list_graphs_names)):
        #print(list_graphs_names[i])
        mat_distance[list_graphs_names[i]]=[]
        for j in range(len(list_graphs_names)):
            mat_distance[list_graphs_names[i]].append(dist_map[i][j])     #geometric_graph_distance(Mats[list_graphs_names[i]],Mats[list_graphs_names[j]],5))
    print("Computing distances has took " + str((time.time() - start)) + "sec")
    print("printing cost matrix")
    file = open(output_file_result,"w") 
    i=0
    for name in list_graphs_names:
        file.write(str(class_labels[i//nb_items_per_class])+":"+str(mat_distance[name])+"\n")
        i+=1
        #print(mat_distance[name])
    print("end")



