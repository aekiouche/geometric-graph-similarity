import networkx as nx 
from lxml import etree as ET
import numpy as np
import math 
from scipy.spatial import distance
from os import walk
import operator
import sys
import time
from scipy.optimize import linear_sum_assignment
import collections


"""                      Input Outpout functions                        """
def read_graph_from_xml(file_path):
    G = nx.Graph()
    nodes={}
    tree = ET.parse(file_path)
    root = tree.getroot()
    id = 0
    for graph in root:
        for element in graph:
            if element.tag == "node":
                nodes[str(element.attrib['id'])]=id
                i = 0
                x_v=y_v=0
                for attrib in element:
                    for value in attrib:
                        if i==0:
                            x_v=float(value.text)
                        else:
                            y_v=float(value.text)
                        i+=1
                G.add_node(id,x=x_v,y=y_v)
                id+=1
            elif element.tag=="edge" :# it's an edge    
                source = nodes[str(element.attrib['from'])]
                target = nodes[str(element.attrib["to"])]
                #distance = float(element.attrib["weight"])
                distance = math.sqrt(math.pow(G.nodes[source]['x']-G.nodes[target]['x'],2) + math.pow(G.nodes[source]['y']-G.nodes[target]['y'],2))
                G.add_edge(source,target,weight=distance)
    distance = math.sqrt(math.pow(G.nodes[0]['x']-G.nodes[len(G.nodes())-1]['x'],2) + math.pow(G.nodes[0]['y']-G.nodes[len(G.nodes())-1]['y'],2))
    G.add_edge(0,len(G.nodes())-1,weight=distance)
    return G
"""                 String edit distance functions                      """

def angle_between(x1,y1,x2,y2):
    angle = math.atan2(x1*y2-y1*x2,x1*x2+y1*y2)
    if (angle < 0 ):
        angle += 2*math.pi
    return angle


def path_to_string(G):
    path_string = []
    edges_list = list(G.edges())
    for  i in range(1,len(edges_list)):
        e_i = edges_list[i-1]
        e_j = edges_list[i]
        x1 =  G.nodes[e_i[0]]['x']- G.nodes[e_i[1]]['x']
        y1 =  G.nodes[e_i[0]]['y']- G.nodes[e_i[1]]['y']
        x2 =  G.nodes[e_j[1]]['x']- G.nodes[e_j[0]]['x']
        y2 =  G.nodes[e_j[1]]['y']- G.nodes[e_j[0]]['y']
        path_string.append(angle_between(x1,y1,x2,y2))
    return path_string

def cost_insertion(a):
    return abs(a) 
def cost_deletion(a):
    return abs(a)
def cost_subtitution(a,b):
    return abs(a-b)



def cyclic_SED(PS1,PS2):
    cyc_sed = string_edit_distance(PS1,PS2)
    for i in range(1,len(PS2)):
        PS3 = collections.deque(PS2)
        PS3.rotate(i)
        PS3 = list(PS3)
        dist = string_edit_distance(PS1,PS3)
        if (dist < cyc_sed):
            cyc_sed = dist
    return cyc_sed
        



def string_edit_distance(PS1,PS2):
    """ Compute the minimum edit distance
    between two strings using the
    Wagner-Fischer algorithm (Dynamic programming)"""

    # Create (m+1)x(n+1) matrix
    cost_matrix = [ [ 0 for j in range(0, len(PS2) +1)] 
              for i in range(0, len(PS1) +1) 
            ]
    # Initialisation
    for i in range(0, len(PS1) +1):
        if i == 0:
            cost_matrix[i][0] = 0
        else:
            cost_matrix[i][0] = cost_deletion(PS1[i-1]) + cost_matrix[i-1][0]

    # Initialisation
    for j in range(0, len(PS2) +1):
        if j==0:
            cost_matrix[0][j] = 0
        else:
            cost_matrix[0][j] = cost_insertion(PS2[j-1]) + cost_matrix[0][j-1]
    for i in range(1, len(PS1) +1):
        for j in range(1, len(PS2) +1):
            S1Index = i - 1
            S2Index = j - 1
            costs= [ cost_matrix[i][j-1] + cost_insertion(PS2[S2Index]),
                     cost_matrix[i-1][j] +  cost_deletion(PS1[S1Index]),
                     cost_matrix[i-1][j-1] + cost_subtitution(PS1[S1Index],PS2[S2Index])
                   ]
            costs.sort()
            cost_matrix[i][j] = costs[0]
    return cost_matrix[len(PS1)][len(PS2)]



if __name__ == "__main__":
    sys.setrecursionlimit(10000)
    sys.setrecursionlimit(10000)
    original_graph_path = sys.argv[1]  
    output_file_result = sys.argv[2]
    nb_items_per_class =  int(sys.argv[3]) 
    
    class_labels={0:"C1",1:"C2",2:"C3",3:"C4",4:"C5",5:"C6",6:"C7",7:"C8",8:"C9",9:"C10"}

    dict_graphs = {}


    print("reading graphs")
    start = time.time()
    for (dirpath, dirname, filenames) in walk(original_graph_path):
        for filename in filenames:
            GG = read_graph_from_xml(original_graph_path+"\\"+filename)
            dict_graphs[filename]= GG
    print("computing distance matrix")
    mat_distance = {}
    start = time.time()
    dist_map={}
    list_graphs_names = list(dict_graphs.keys())
    dict_strings = {}
    for i in range(len(list_graphs_names)):
        dict_strings[list_graphs_names[i]] = path_to_string(dict_graphs[list_graphs_names[i]])
        dist_map[i]={}

    for i in range(len(list_graphs_names)):
        print(i)
        dist_map[i][i]=0
        for j in range(i+1,len(list_graphs_names)):
            dij = cyclic_SED(dict_strings[list_graphs_names[i]],dict_strings[list_graphs_names[j]])
            dist_map[i][j]=dij
            dist_map[j][i]=dij

    for i in range(len(list_graphs_names)):
        #print(list_graphs_names[i])
        mat_distance[list_graphs_names[i]]=[]
        for j in range(len(list_graphs_names)):
            mat_distance[list_graphs_names[i]].append(dist_map[i][j])     
    print("Computing distances has took " + str((time.time() - start)) + "sec")
    print("printing cost matrix")
    file = open(output_file_result,"w") 
    label={0:"C1",1:"C2",2:"C3",3:"C4",4:"C5",5:"C6",6:"C7",7:"C8",8:"C9",9:"C10"}
    i=0
    for name in list_graphs_names:
        file.write(str(class_labels[i//nb_items_per_class])+":"+str(mat_distance[name])+"\n")
        i+=1
        #print(mat_distance[name])
    print("end")